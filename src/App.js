import GlobalStyle from "./styles/globalStyle";
import Dashboard from "./components/Dashboard/Dashboard"

const App = () => {
  return (
    <div className="App">
      <GlobalStyle />
      <Dashboard />
    </div>
  );
}

export default App;
