import { useEffect, useState } from "react";
import Contact from "../Contact/Contact";
import SearchBar from "../SearchBar/SearchBar";
import StyledHeader from "./Dashboard.style";

const Dashboard = () => {
  const [sortedUsers, setSortedUsers] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  const isSearched = (str) =>
    str.toLowerCase().indexOf(searchText.toLowerCase()) > -1;

  const toggleCheckbox = (id) => {
    setSortedUsers(() =>
      sortedUsers.map((user) =>
        id === user.id ? { ...user, isSelected: !user.isSelected } : user
      )
    );
  };

  const getUsers = async () => {
    try {
      const res = await fetch(
        "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
      );
      const users = await res.json(res);
      setLoading(() => false);
      setSortedUsers(() =>
        users
          .sort((a, b) => (a.last_name < b.last_name ? -1 : 1))
          .map((user) => ({ ...user, isSelected: false }))
      );
    } catch (e) {
      setError(true);
    }
  };

  useEffect(() => {
    getUsers();
  }, []);

  useEffect(() => {
    console.log(
      sortedUsers.filter((user) => user.isSelected).map((user) => user.id)
    );
  }, [sortedUsers]);

  const searchedContacts = sortedUsers
    .filter((user) => isSearched(`${user.first_name} ${user.last_name}`))
    .map((user) => {
      return (
        <Contact
          key={user.id}
          id={user.id}
          avatar={user.avatar}
          name={`${user.first_name} ${user.last_name}`}
          isSelected={user.isSelected}
          toggleCheckbox={toggleCheckbox}
        />
      );
    });

  return (
    <>
      <StyledHeader>
        <h1>Contacts</h1>
      </StyledHeader>
      <SearchBar setSearchText={setSearchText} />
      {error ? (
        <span>Couldn't fetch users</span>
      ) : loading ? (
        <span>Loading...</span>
      ) : (
        searchedContacts
      )}
    </>
  );
};

export default Dashboard;
