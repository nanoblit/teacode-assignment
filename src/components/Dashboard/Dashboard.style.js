import styled from "styled-components";

const StyledHeader = styled.header`
  width: 100%;
  padding: 2rem 0;
  background: linear-gradient(90deg, rgba(98,189,218,1) 0%, rgba(92,208,129,1) 100%);
  border-bottom: 2px solid lightgray;

  h1 {
    color: white;
    text-align: center;
    font-weight: bold;
    font-size: 2rem;
  }
`;

export default StyledHeader;