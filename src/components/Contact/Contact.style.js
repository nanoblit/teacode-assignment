import styled from "styled-components";

const StyledContact = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  border-bottom: 2px solid lightgray;
  padding: 1rem 2rem;
  cursor: pointer;

  .user-block {
    width: 30rem;
    display: flex;
    align-items: center;

    img {
      border: 4px solid lightgray;
      background-color: lightblue;
      width: 54px;
      height: 54px;
      border-radius: 100%;
    }

    span {
      margin-left: 1rem;
    }
  }

  .checkbox {
    display: block;
    position: relative;
    cursor: pointer;
    font-size: 22px;
    width: 25px;
    height: 25px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;

    input {
      opacity: 0;
      position: absolute;
      top: 0;
      left: 0;
      margin: 0;
      cursor: pointer;
      height: 25px;
      width: 25px;
      z-index: 1;
    }

    .checkmark {
      border-radius: 4px;
      position: absolute;
      top: 0;
      left: 0;
      height: 25px;
      width: 25px;
      background-color: #c8dfe6;

      &:after {
        content: "";
        position: absolute;
        display: none;
        left: 7.5px;
        top: 4px;
        width: 8px;
        height: 15px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
      }
    }

    &:hover input ~ .checkmark{
      background-color: lightblue;
    }

    input:checked ~ .checkmark {
      background-color: #61b0ca;
    }

    input:checked ~ .checkmark:after {
      display: block;
    }

    input:focus ~ .checkmark {
      background-color: lightblue;
    }
  }
`;

export default StyledContact;
