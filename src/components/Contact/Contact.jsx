import StyledContact from "./Contact.style";

const Contact = ({ id, avatar, name, isSelected, toggleCheckbox }) => {
  return (
    <StyledContact onClick={() => toggleCheckbox(id)}>
      <div className="user-block">
        <img alt="avatar" src={avatar} />
        <span>{name}</span>
      </div>
      <div className="checkbox">
        <input
          type="checkbox"
          checked={isSelected}
          aria-label={`Check ${name}`}
          onChange={() => toggleCheckbox(id)}
        />
        <span className="checkmark"></span>
      </div>
    </StyledContact>
  );
};

export default Contact;
