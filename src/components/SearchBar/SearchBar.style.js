import styled from "styled-components";

const StyledSearchBar = styled.input`
  width: 100%;
  display: inline-block;
  border: none;
  padding: 1rem 2rem;
  outline: none;
  border-bottom: 2px solid lightgray;
`;

export default StyledSearchBar;