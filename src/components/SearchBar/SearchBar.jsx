import StyledSearchBar from "./SearchBar.style";
import debounce from "lodash.debounce";
import { useRef, useState } from "react";

const SearchBar = ({ setSearchText }) => {
  const [text, setText] = useState("");
  const debounceSearch = useRef(
    debounce((str) => {
      setSearchText(str);
    }, 500)
  ).current;

  const changeText = (str) => {
    setText(() => str);
    debounceSearch(str);
  };

  return (
    <StyledSearchBar
      value={text}
      type="text"
      placeholder="Search"
      onChange={(e) => changeText(e.target.value)}
    />
  );
};

export default SearchBar;
