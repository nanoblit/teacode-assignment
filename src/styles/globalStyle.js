import { createGlobalStyle } from "styled-components";
import reset from "./reset";

const GlobalStyle = createGlobalStyle`
  ${reset}

  html {
    font-size: 62.5%;
  }

  body, input {
    font-weight: bold;
    font-size: 1.6rem; 
    font-family: 'Roboto', sans-serif;
  }
`;

export default GlobalStyle;
